<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .container {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 34vw;
    margin: 2rem 33vw;
    border: 2px solid #385e8b;
    padding: 1rem;
  }
</style>

<body>
  <div class="container">
    <?php
    if (isset($_COOKIE['total_results_page1']) && isset($_COOKIE['total_results_page2'])) {
      $result = (int)$_COOKIE['total_results_page1'] + (int)$_COOKIE['total_results_page2'];

      echo "<p style='margin: 0px'>Số câu đúng là:  $result </p>";
      echo "<p>Điểm: $result </p>";
      if ($result < 4) {
        echo "Bạn quá kém, cần ôn tập thêm";
      } else if ($result < 7) {
        echo "Cũng bình thường";
      } else {
        echo "Sắp sửa làm được trợ giảng lớp PHP";
      }
    }
    ?>
  </div>
</body>

</html>
